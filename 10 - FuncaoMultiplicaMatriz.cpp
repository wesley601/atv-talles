#include <iostream>
#include <string>
using namespace std;
/*
Crie uma função capaz de multiplicar uma linha de uma matriz por um dado número. 
Faça o mesmo para uma coluna.
*/
int **multiplicaLinhaColuna(int ln, int col, int linha, 
int coluna, int v1, int v2, int **matriz){
    
    for(int i = 0; i < ln; i++){
        matriz[linha][i] *= v1;
    }
    for(int j = 0; j < col; j++){
        matriz[j][coluna] *= v2;
    }
    return matriz;
}
 
int main()
{
    int **matriz, v1, v2;
    int ln, col, linha, coluna;
    cout << "Digite o tamanho da matriz linha coluna" << endl;
    cin >> ln;
    cin >> col;

    matriz = (int **) malloc(ln * sizeof(sizeof(int)));
    
    for(int i = 0; i < col; i++){
        matriz[i] = (int *) malloc(col * sizeof(int));
    }

    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            cout << "Digite o valor da posição " << i << j << endl;
            cin >> matriz[i][j];
        }
        cout << endl;
    }
    cout << "Digite a linha que quer multiplicar"<< endl;
    cin >> linha;
    cout << "Digite o multiplicador" << endl;
    cin >> v1;
    
    cout << "Digite a coluna que quer multiplicar"<< endl;
    cin >> coluna;
    cout << "Digite o multiplicador" << endl;
    cin >> v2;

    
    matriz = multiplicaLinhaColuna(ln, col, linha, coluna, v1, v2, matriz);
    
    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }

    
    return 0;
}
