#include <iostream>
#include <cstring>
using namespace std;

/*
Crie um progama capaz de ler dois nomes de pessoas e imprimi-los em ordem alfabética. 
Faça isto com string de C e de C++
*/

int main (){

char nm[3][20], aux[20];

for (int i = 0; i < 3; i++) {
   cout << "Digite o nome" << endl;
   fgets(nm[i], 20, stdin);
}

for (int i = 1; i < 3; i++) { /* 3 = qtde de palavras */
   for (int j = 1; j < 3; j++) {
      // verifica se tem que ser depois, se for troca de posição
      if (strcmp(nm[j - 1], nm[j]) > 0) {
         strcpy(aux, nm[j - 1]);
         strcpy(nm[j - 1], nm[j]);
         strcpy(nm[j], aux);
      }
   }
}

// só mostrar a matriz
for (int i = 0; i < 3; i++)
    cout << nm[i] << endl;


}

