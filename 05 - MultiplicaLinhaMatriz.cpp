#include <iostream>
#include <stdlib.h>
#include <string>
using namespace std;
/*
5. Crie um programa capaz de multiplicar uma linha de uma matriz de inteiros por um dado número. 
Faça o mesmo para uma coluna. A matriz deve ser lida de teclado.

void imprimeMatriz(int matriz){}
*/


int main()
{
    int *ln = (int *) malloc(sizeof(int));
    int *col = (int *) malloc(sizeof(int));

    int matriz[*ln][*col];
    int linha, coluna, v1, v2; //v são os multiplicadores
    
    cout << "Digite o tamanho da matriz \n linha coluna" << endl;
    cin >> *ln;
    cin >> *col;

    for(int i = 0; i < *ln; i++){
        for(int j = 0; j < *col; j++){
            cout << "Digite o valor da posição " << i << j << endl;
            cin >> matriz[i][j];
        }
        cout << endl;
    }
    
    for(int i = 0; i < *ln; i++){
        for(int j = 0; j < *col; j++){
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }

    cout << "Digite a linha que quer multiplicar"<< endl;
    cin >> linha;
    cout << "Digite o multiplicador" << endl;
    cin >> v1;
    
    for(int i = 0; i < *ln; i++){
        matriz[linha][i] *= 2;
    }
    cout << "Digite a coluna que quer multiplicar"<< endl;
    cin >> coluna;
    cout << "Digite o multiplicador" << endl;
    cin >> v2;

    for(int j = 0; j < *col; j++){
        matriz[j][coluna] *= 2;
    }
    cout << "\n\n";
    
    for(int i = 0; i < *ln; i++){
        for(int j = 0; j < *col; j++){
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}
