#include <iostream>
#include <string>
using namespace std;
/*
Crie um programa capaz de ler os dados de uma matriz quadrada de inteiros. Ao final da leitura o programa deverá imprimir o número da linha que contém o menor dentre todos os números lidos.
*/
int main()
{
    int menor = 2147483647, linha, x, y;
    cout << "Digite a linha" << endl;
    cin >> x;
    cout << "Digite a coluna" << endl;
    cin >> y;
    int matriz[x][y];

    for(int i = 0; i < x; i++){
        for(int j = 0; j < y; j++){
            
            cout << "Digite o valor:" << endl;
            cin >> matriz[i][j];
            
            if(matriz[i][j] < menor){
                menor = matriz[i][j];
                linha = j;
            }
        }
    }
    cout << "A linha que contêm o menor valor é a " << linha+1 << ", e o valor é " << menor << endl;
    return 0;
}
