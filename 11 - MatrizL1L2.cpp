#include <iostream>
#include <string>
/*
11. Crie uma função capaz de somar os elementos das linhas L1 e L2 de uma matriz. 
O resultado deve ser colocado na linha L2. Faça o mesmo com a multiplicação.
*/
using namespace std;

void l1somal2(int ln, int col, int **x)
{
    for(int i = 0; i < col; i++){
            x[i][1] += x[i][0]; 
    }
    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
        cout << x[i][j] << " "; 
        }
        cout << endl;
    }
        for(int i = 0; i < col; i++){
            x[i][1] -= x[i][0]; 
    }
}
void l1muiltl2(int ln, int col, int **y)
{
    for(int i = 0; i < col; i++){
        y[i][1] *= y[i][0]; 
    }
    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            cout << y[i][j] << " "; 
        }
            cout << endl;
    }
    for(int i = 0; i < col; i++){
        y[i][1] /= y[i][0]; 
    }
}
int main(){
    int **matriz;
    int ln, col;
    cout << "Digite o tamanho da matriz linha coluna" << endl;
    cin >> ln;
    cin >> col;
    matriz = (int **) malloc(ln * sizeof(sizeof(int)));
    for(int i = 0; i < col; i++){ matriz[i] = (int *) malloc(col * sizeof(int)); }
    
   for(int i = 0; i < ln; i++){
       for(int j = 0; j < col; j++){
           cout << "Digite o valor da posição " << i << j << endl;
           cin >> matriz[i][j];
       }
        cout << endl;
    }

    l1somal2(ln, col, matriz);
    l1muiltl2(ln, col, matriz);
    
    cout << endl;
    
    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            cout << matriz[i][j] << " "; 
        }
            cout << endl;
    }
    return 0;
}