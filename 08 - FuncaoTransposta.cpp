#include <iostream>
#include <string>
using namespace std;
/*
Crie uma função capaz de criar a transposta de uma matriz.
*/
void transposta(int ln, int col, int **matriz){

    //int matriz[ln][col];
    int i = 0;
    int j = 0;

    while( i < ln ){
        for( ; j < col; j++){
        int tmp;      
        tmp = matriz[j][i];
        matriz[j][i] = matriz[i][j];
        matriz[i][j] = tmp;
        }

        i++;
        j = i + 1;
    }
    cout << endl;
     for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
    
}

int main()
{
    
    int **matriz, tmp;
    int mt;
    cout << "Digite o valor da matriz quadratica" << endl;
    cin >> mt;
    
    matriz = (int **) malloc(mt * sizeof(sizeof(int *)));
    
    for(int n = 0; n < mt; n++){
        matriz[n] = (int *) malloc(mt *sizeof(int));
    }

    for(int i = 0; i < mt; i++){
        for(int j = 0; j < mt; j++){
            cout <<"Digite o valor da coordenada " << i <<" "<< j << endl;
            cin >> tmp;
            matriz[i][j] = tmp;
        }
        cout << endl;
    }
    
   transposta(mt, mt, matriz);
    
    return 0;
}
