#include <iostream>
using namespace std;
/*
Escreva um programa que leia 3 notas de um aluno e a média das notas dos exercícios realizados
por ele. Calcular a média de aproveitamento, usando a fórmula: MA = (N1 + N2*2 + N3*3 + ME)/7. 
A partir da média, informar o conceito de acordo com a tabela: 
maior ou igual a 9
A
maior ou igual a 7.5 e menor que 9
B
maior ou igual a 6 e menor que 7.5
C
maior ou igual a 4 e menor que 6
D
menor que 4
E
*/
int main()
{
    float n1, n2, n3, me, ma, exec;
    int execs; //Numero de exercicíos realizados
    cout << "Digite a 1º nota" << endl;
    cin >> n1;
    cout << "Digite a 2º nota" << endl;
    cin >> n2;
    cout << "Digite a 3º nota" << endl;
    cin >> n3;
    
    cout << "Quantos exercicíos foram realizados?" << endl;
    cin >> execs;

    
    for(int i = 0; i < execs; i++)
    {
        cout << "Digite a nota do " << i+1 << "º exercicío" << endl;
        cin >> exec;
        me += exec;

    }
    ma = (n1 + n2*2 + n3*3 + me)/7;

    if(ma >= 9)
    {
        cout << "Nota A" << endl;
    } 
        else if(ma >= 7.5 || ma < 9)
        {
            cout << "Nota B" << endl;
        } 
            else if(ma >= 6 || ma < 7.5)
            {
                cout << "Nota C" << endl;
            } 
              else if(ma >= 4 || ma < 6)
              {
                  cout << "Nota D" << endl;
              } 
                    else if(ma < 4)
                    {
                        cout << "Nota E" << endl;
                    }
    return 0;
}
