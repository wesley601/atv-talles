#include <iostream>
#include <string>
using namespace std;
/*
Faça um programa que crie um vetor de pessoas. 
Os dados de uma pessoa devem ser armazenados em um variavel do tipo struct.
O programa deve permitir que o usuário digite o nome de 3 pessoas e a seguir 
imprimi os dados de todas as pessoas. A struct deve armazenar os dados de idade, peso e altura.
*/
struct dados {
    string nome;
    int idade;
    float peso;
    float altura;
};

int main()
{
    dados pessoas[3];

    
    for(int i = 0; i < 3; i++)
    {
        cout << "Digite o nome da " << i+1 <<"º pessoa" << endl;
        cin >> pessoas[i].nome;
        cout << "Sua idade" << endl;
        cin >> pessoas[i].idade;
        cout << "Seu peso" << endl;
        cin >> pessoas[i].peso;
        cout << "Sua altura" << endl;
        cin >> pessoas[i].altura;
    }
    
    for(int i = 0; i < 3; i++)
    {
        cout << pessoas[i].nome << " tem " << pessoas[i].idade <<
        " de idade, pesa " << pessoas[i].peso << "kg e mede " << pessoas[i].altura << endl; 
    }
    
    return 0;
}
