#include <iostream>
#include <string>
using namespace std;
/*
Crie um programa capaz de criar a transposta de uma matriz. A matriz deve ser lida de teclado.
*/
int main()
{
    int mt;
    cout << "Digite o valor da matriz quadratica" << endl;
    cin >> mt;

    int matriz[mt][mt];// x serve para percorrer as colunas
    
    for(int i = 0; i < mt; i++){
        for(int j = 0; j < mt; j++){
            cout <<"Digite o valor da coordenada " << i <<" "<< j << endl;
            cin >> matriz[i][j];
        }
        cout << endl;
    }
    int i = 0;
    int j = 0;
    while( i < mt ){
        for( ; j < mt; j++){
        int tmp;      
        tmp = matriz[j][i];
        matriz[j][i] = matriz[i][j];
        matriz[i][j] = tmp;
        }

        i++;
        j = i + 1;
    }
    cout << endl;
     for(int i = 0; i < mt; i++){
        for(int j = 0; j < mt; j++){
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
    
/*
    for(int i = 0; i < 3; i++){
        int tmp;
        int j = 0;
        
        tmp = matriz[j][i];
        matriz[j][i] = matriz[i][j];
        matriz[i][j] = tmp;
        
        j++;
        if(j == 2){
            j = 0;
        }
    }
     
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
        int temp;
        tmp = matriz[j][i+x];
        matriz[j][i+x] = matriz[i+x][j];
        matriz[i+x][j] = tmp;
        }
    }
    */
    return 0;
}
