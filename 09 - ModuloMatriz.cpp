#include <iostream>
#include <string>
using namespace std;
/*
Crie uma função capaz de substituir todos os números negativos de uma matriz por seu módulo.
*/
int **modulo(int ln, int col, int **matriz){
    
    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            if(matriz[i][j] < 0){
                matriz[i][j] *= (-1);
            }
        }
    }
    return matriz;    
}

int main()
{
    int **matriz;
    int ln, col;
    cout << "Digite a linha" << endl;
    cin >> ln;
    cout << "Digite a coluna" << endl;
    cin >> col;

    matriz = (int **) malloc(ln * sizeof(sizeof(int)));

    for(int i = 0; i < col; i++){
        matriz[i] = (int *) malloc(col * sizeof(int));
    }
    
    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            cout <<"Digite o valor da coordenada " << i <<" "<< j << endl;
            cin >> matriz[i][j];
        }
        cout << endl;
    }

    matriz = modulo(ln, col, matriz);

    for(int i = 0; i < ln; i++){
        for(int j = 0; j < col; j++){
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}
